* Use a natural sort order of number, to e.g. get 1,2,10 instead of 1,10,2 (#25).
* Put task notification toast at top of screen to avoid blocking toasts shown from termux-toast #33).
